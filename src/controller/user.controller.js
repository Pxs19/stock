const userService = require("../service/user.service");
const productService = require("../service/product.service");
const mongoose = require("../configs/db");

exports.getUsers = async (req, res, next) => {
  try {
    const result = await userService.getUsers();
    return res.status(200).send({
      status: 200,
      message: "success",
      data: result,
    });
  } catch (error) {
    return res.status(500).send({ message: "server error", success: false });
  }
};

exports.addUser = async (req, res, next) => {
  console.log(req.body);
  try {
    const result = await userService.addUser(req.body);
    return res.status(201).send({
      status: 201,
      message: "success",
      token: result,
    });
  } catch (error) {
    return res.status(500).send({ message: "create fail", success: false });
  }
};

exports.adminApprove = async (req, res, next) => {
  try {
    const result = await userService.adminApprove(req.params.id, req.body);
    return res.status(200).send({
      status: 200,
      message: "update success",
      data: result,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: "create fail", success: false });
  }
};

exports.getProductsByUserId = async (req, res, next) => {
  try {
    console.log("ppppppppppppppppppppppppppppppppp");
    const userId = req.user._id;
    console.log(userId);
    const id = userId.valueOf().toString();
    const result = await productService.getProductsByUserId(userId);

    return res.status(200).send({
      status: 200,
      message: "success",
      data: result,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .send({ status: 500, message: "server error", success: false });
  }
};
