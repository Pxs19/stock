const orderService = require("../service/order.service");
const paymentService = require("../service/payment.service");

exports.getOrders = async (req, res, next) => {
  try {
    const userId = req.user._id;
    const result = await orderService.getOrders(userId);
    return res.status(200).send({
      status: 200,
      message: "success",
      data: result,
    });
  } catch (error) {
    return res.status(500).send({ message: "server error", success: false });
  }
};

exports.createOrder = async (req, res, next) => {
  try {
    // const userId = req.FROM MIDDLEWARE
    const user = req.user._id;
    // console.log(user);
    // console.log(req.body);
    const result = await orderService.createOrder(user, req.body);
    // console.log(result);
    console.log("Here ===================================>");

    if (Array.isArray(result)) {
      if (result[0] == "User not found") {
        return res.status(404).send({ status: 404, message: "user not found" });
      } else if (result.length > 0) {
        return res.status(404).send({
          status: 500,
          message: "product out of stock",
          product: result,
        });
      }
    }

    // const oauth = await orderService.payment(result);
    const qr = await paymentService.payment(result);

    return res.status(201).send({
      status: 201,
      message: "create success",
      data: qr,
    });
  } catch (error) {
    // console.log(error);
    console.log(error);
    return res.send({ message: error });
  }
};
