const authService = require("../service/auth.service");

exports.signUp = async (req, res, next) => {
  console.log(req.body);
  try {
    const result = await authService.signUp(req.body);
    return res.status(result.status).send(result);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: "create fail", success: false });
  }
};

exports.login = async (req, res, next) => {
  try {
    const result = await authService.login(req.body);
    console.log(result);
    return res.status(result.status).send(result);
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: "create fail", success: false });
  }
};
