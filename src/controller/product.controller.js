const productService = require("../service/product.service");
const mongoose = require("../configs/db");

exports.getProducts = async (req, res, next) => {
  try {
    const result = await productService.getProducts();
    // console.log("hhjhj");
    console.log(req.user);
    // console.log("hhjhj");
    return res.status(200).send({
      status: 200,
      message: "success",
      data: result,
    });
  } catch (error) {
    return res
      .status(500)
      .send({ status: 500, message: "server error", success: false });
  }
};

exports.getProduct = async (req, res, next) => {
  console.log(req.params.id);
  const result = await productService.getProduct(req.params.id);
  console.log(result);
  try {
    const productId = req.params.id;
    const result = await productService.getProduct(productId);
    return res.status(200).send({
      status: 200,
      message: "success",
      data: result,
    });
  } catch (error) {
    return res
      .status(500)
      .send({ status: 500, message: "server error", success: false });
  }
};

exports.getProductsByUserWhoadded = async (req, res, next) => {
  try {
    const userId = req.params.id;
    const result = await productService.getProductsByUserWhoadded(userId);
    return res.status(200).send({
      status: 200,
      message: "success",
      data: result,
    });
  } catch (error) {
    return res
      .status(500)
      .send({ status: 500, message: "server error", success: false });
  }
};

exports.addProduct = async (req, res, next) => {
  try {
    const userId = req.user._id;
    const data = req.body;
    // console.log(userId);
    data.user = userId.valueOf().toString();
    // console.log(data);
    const result = await productService.addProduct(req.body);
    return res.status(201).send({
      status: 201,
      message: "add success",
      data: result,
    });
  } catch (error) {
    return res
      .status(500)
      .send({ status: 500, message: "create fail", success: false });
  }
};

exports.getProductWithOrder = async (req, res, next) => {
  try {
    const result = await productService.getProductWithOrder(req.params.id);

    res.status(200).send({ status: 201, message: "success", data: result });
  } catch (error) {
    return res
      .status(500)
      .send({ status: 500, message: "create fail", success: false });
  }
};

exports.updateProduct = async (req, res, next) => {
  try {
    const checkProduct = await productService.getProduct(req.params.id);
    if (!checkProduct)
      return res
        .status(404)
        .send({ status: 404, message: "Product not found", data: [] });

    const result = await productService.updateProduct(req.params.id, req.body);
    return res.status(200).send({
      status: 200,
      message: "update success",
      data: result,
    });
  } catch (error) {
    return res
      .status(500)
      .send({ status: 500, message: "server error", success: false });
  }
};

exports.deleteProduct = async (req, res, next) => {
  try {
    const checkProduct = await productService.getProduct(req.params.id);
    if (!checkProduct)
      return res
        .status(404)
        .send({ status: 404, message: "Product not found", data: [] });

    const result = await productService.deleteProduct(req.params.id);
    return res.status(200).send({
      status: 200,
      message: "delete success",
      data: result,
    });
  } catch (error) {
    return res
      .status(500)
      .send({ status: 500, message: "server error", success: false });
  }
};

exports.orderProduct = async (req, res, next) => {
  try {
    // const userId = req.FROM MIDDLEWARE
    const { user } = req.body;
    const result = await productService.orderProduct(user, req.body);

    if (Array.isArray(result)) {
      if (result[0] == "User not found") {
        return res.status(404).send({ status: 404, message: "user not found" });
      } else if (result.length > 0) {
        return res.status(404).send({
          status: 500,
          message: "product out of stock",
          product: result,
        });
      }
    }
    return res.status(201).send({
      status: 201,
      message: "create success",
      data: result,
    });
  } catch (error) {
    return res
      .status(500)
      .send({ status: 500, message: "server error", success: false });
  }
};

exports.uselessCreateOrder = async (req, res, next) => {
  try {
    // const userId = req.FROM MIDDLEWARE
    const userId = req.user._id.valueOf().toString();
    const result = await productService.uselessCreateOrder(
      req.params.id,
      userId,
      req.body
    );
    console.log("====LLl");
    console.log(result);

    if (Array.isArray(result)) {
      if (result[0] == "User not found") {
        return res.status(404).send({ status: 404, message: "user not found" });
      } else if (result[0] == "Add some Product") {
        return res
          .status(404)
          .send({ status: 404, message: "Add some Product" });
      } else if (result.length > 0) {
        return res.status(404).send({
          status: 500,
          message: "product out of stock",
          product: result,
        });
      }
    }

    return res
      .status(201)
      .send({ status: 201, message: "Order complete", data: [result] });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .send({ status: 500, message: "server error", success: false });
  }
};
