const mongoose = require("../configs/db");
const Products = new mongoose.Schema(
  {
    image: {
      type: String,
      default:
        "https://mercular.s3.ap-southeast-1.amazonaws.com/images/products/2022/08/Product/razer-deathadder-v3-pro-wireless-gaming-mouse-cover-view.jpg",
    },
    product_name: { type: String },
    price: { type: Number },
    amount: { type: Number },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
    orders: [{ type: mongoose.Schema.Types.ObjectId, ref: "OrderedProducts" }],
  },
  { timestamps: true }
);

module.exports = mongoose.model("Products", Products);
