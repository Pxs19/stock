const mongoose = require("../configs/db");

const Orders = new mongoose.Schema({
  user: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
  products: [{ type: mongoose.Schema.Types.ObjectId, ref: "OrderedProducts" }],
  // products: [{ type: mongoose.Schema.Types.ObjectId, ref: "products" }],
  status: { type: String, default: "NOT COMPLETE" },
});

module.exports = mongoose.model("Orders", Orders);
