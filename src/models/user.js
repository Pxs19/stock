const mongoose = require("../configs/db");

const Users = new mongoose.Schema(
  {
    username: { type: String, unique: true },
    password: { type: String },
    email: { type: String },
    firstname: { type: String },
    lastname: { type: String },
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: "Products" }],
    orders: [{ type: mongoose.Schema.Types.ObjectId, ref: "Orders" }],
    role: { type: Array, default: ["user"] },
    status: { type: String, default: "NOT APPROVE" },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Users", Users);
