const mongoose = require("../configs/db");
const Users = require("./user");
const Products = require("./product");
const hashFunction = require("../utils/passHash");

const product = {
  product_name: "TestProduct",
  price: 35000,
  amount: 20,
};

const user = {
  username: "TestUser",
  password: "1234",
  email: "test@example.com",
  firstname: "TestFirstname",
  lastname: "TestLastname",
  role: ["user"],
};

async () => {
  try {
    console.log("<==== Im seeding ====> ");
    const { username, password, email, firstname, lastname, role } = user;
    const passHash = await hashFunction(password);
    let seedUser = new Users({
      username,
      passHash,
      email,
      firstname,
      lastname,
      role,
    });
    await seedUser.save();
    console.log("<==== Im seeding ====> ");
    const { product_name, price, amount } = product;
    let seedProduct = new Products({ product_name, price, amount });
    await seedProduct.save();
  } catch (error) {
    console.log(error);
  }
};
