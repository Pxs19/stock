const mongoose = require("../configs/db");

const OrderedProducts = new mongoose.Schema({
  product: { type: mongoose.Schema.Types.ObjectId, ref: "Products" },
  orderId: { type: mongoose.Schema.Types.ObjectId, ref: "Orders" },
  quantity: { type: Number },
});

module.exports = mongoose.model("OrderedProducts", OrderedProducts);
