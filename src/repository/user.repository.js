const mongoose = require("../configs/db");
const User = require("../models/user");
const hashFunction = require("../utils/passHash");
const bcrypt = require("bcrypt");
const jwt = require("../configs/jwt");

exports.getUsers = async () => {
  const users = await User.find();
  return users;
};

exports.addUser = async (userData) => {
  try {
    // console.log(userData);
    const { username, password, email, firstname, lastname } = userData;
    let hashPass = await hashFunction(password);

    const newUser = new User({
      username,
      password: hashPass,
      email,
      firstname,
      lastname,
    });
    let user = await newUser.save();
    return {
      status: 201,
      success: true,
      message: "User registered successfully",
      data: user,
    };
  } catch (error) {
    console.error(error);
    return {
      success: false,
      status: 500, // Internal Server Error
      message: "Failed to register user",
      error: error.message,
    };
  }
};

exports.login = async (userData) => {
  const { username, password } = userData;

  try {
    // check user in system
    const user = await User.findOne({ username: username });
    // console.log(user);
    if (!user)
      return { status: 404, success: false, message: "User not in system" };

    // check password mathc
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch)
      return {
        status: 401,
        success: false,
        message: "Password does not match",
      };
    if (user.status === "NOT APPROVE")
      return { status: 403, success: false, message: "Wait for approval" };

    const payload = {
      _id: user._id,
      username: user.username,
      role: user.role,
      status: user.status,
      // test: "test",
    };
    // sign part
    const token = await jwt.signToken(payload);

    return { status: 200, success: true, token: token, role: user.role };
  } catch (error) {
    console.error(error);
    return {
      status: 500,
      success: false,
      message: "An error occurred during login",
    };
  }
};

exports.adminApprove = async (userId, approveData) => {
  const { status } = approveData;

  const updated = await User.findByIdAndUpdate(
    userId,
    { status },
    { new: true }
  );
  return updated;
};
