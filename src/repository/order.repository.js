const mongoose = require("../configs/db");
const product = require("../models/product");
const Product = require("../models/product");
const User = require("../models/user");
const OrderedProducts = require("../models/productinorder");
const Order = require("../models/order");

// get all orders
exports.getOrders = async (userId) => {
  const orders = await Order.find({ user: userId }).populate("products");
  //
  // more order detail
  // const orders = await Order.find().populate({
  //   path: "products",
  //   populate: {
  //     path: "product",
  //     model: "Products",
  //     populate: {
  //       path: "user",
  //       model: "Users",
  //     },
  //   },
  // });
  console.log(orders);
  return orders;
};

exports.createOrder = async (userId, orderData) => {
  // userId -> id from middleware
  const { products } = orderData;
  let newzOrder = new Order({ user: userId });
  const overInStock = [];
  if (products && products.length > 0) {
    const getProducts = products.map((products) => {
      return new OrderedProducts({ ...products, orderId: newzOrder._id });
    });
    var getProductOrderedId = [];

    // console.log(getProducts[0].product);
    for (let index = 0; index < getProducts.length; index++) {
      //   console.log(getProducts[index].product);
      var productId = getProducts[index].product.valueOf().toString();
      var userQuantity = getProducts[index].quantity;

      var productFromStock = await Product.findById(productId);

      if (userQuantity > productFromStock.amount) {
        overInStock.push(productFromStock.product_name);
      }

      //   getProductOrderedId.push(getProducts[index]._id);
      getProductOrderedId[0] = getProducts[index]._id;
      let getProductsORders = await Product.findById(productId);
      getProductsORders.orders.push(getProductOrderedId);
      await getProductsORders.save();
    }

    if (overInStock.length > 0) {
      console.log("Overrrrr");
      return overInStock;
    } else {
      await Product.findByIdAndUpdate(productId, {
        amount: productFromStock.amount - userQuantity,
      });

      //   console.log(getProductOrderedId);
      //   console.log(productId);

      //   const updateProductsORders = await Product.?????(productId, {
      //     orders: getProductOrderedId,
      //   });

      //   let getProductsORders = await Product.findById(productId);
      //   //   console.log(getProductsORders.orders);
      //   getProductsORders.orders.push(getProductOrderedId);
      //   await getProductsORders.save();

      await OrderedProducts.insertMany(getProducts);
      newzOrder.products = getProducts.map((products) => products._id);
      await newzOrder.save();

      let getUser = await User.findById(userId);

      if (!getUser) {
        console.log("User not found");
        return ["User not found"];
      }

      if (!getUser.orders) {
        getUser.orders = [];
      }

      getUser.orders.push(newzOrder._id);
      await getUser.save();

      return "Hello";
    }

    // return getUser;
  }

  return "none";
};

exports.createSuckOrder = async (userId, orderData) => {
  const { products } = orderData;
  let newzOrder = new Order({ user: userId });
  const overInStock = [];
  var getProductOrderedId = [];

  if (products && products.length > 0) {
    const getProducts = products.map((products) => {
      return new OrderedProducts({ ...products, orderId: newzOrder._id });
    });

    for (let index = 0; index < getProducts.length; index++) {
      //   console.log(index);
      let productId = getProducts[index].product.valueOf().toString();
      let userQuantity = getProducts[index].quantity;

      const productFromStock = await Product.findById(productId);

      if (userQuantity > productFromStock.amount) {
        overInStock.push(productFromStock.product_name);
      }
    }

    if (overInStock.length > 0) {
      console.log(overInStock);
      return overInStock;
    }

    // return ":)))))";
  }

  if (products && products.length > 0) {
    let getUser = await User.findById(userId);

    if (!getUser) {
      console.log("User not found");
      return ["User not found"];
    }

    const getProducts = products.map((products) => {
      return new OrderedProducts({ ...products, orderId: newzOrder._id });
    });

    for (let index = 0; index < getProducts.length; index++) {
      let productId = getProducts[index].product.valueOf().toString();
      let userQuantity = getProducts[index].quantity;

      const productFromStock = await Product.findById(productId);
      getProductOrderedId[0] = getProducts[index]._id;
      let getProductsOrders = await Product.findById(productId);
      getProductsOrders.orders.push(getProductOrderedId);
      await getProductsOrders.save();

      // update amount in db
      await Product.findByIdAndUpdate(productId, {
        amount: productFromStock.amount - userQuantity,
      });
    }

    await OrderedProducts.insertMany(getProducts);
    newzOrder.products = getProducts.map((products) => products._id);
    await newzOrder.save();

    if (!getUser.orders) {
      getUser.orders = [];
    }

    getUser.orders.push(newzOrder._id);
    await getUser.save();

    // return newzOrder.populate("products");
    return newzOrder.populate({
      path: "products",
      populate: {
        path: "product",
        model: "Products",
      },
    });

    // const newPayload = {};
  }
};

// send productId, userId from middleware, productData from body
exports.uselessCreateOrder = async (productId, userId, orderData) => {
  console.log(productId);
  const { quantity } = orderData;
  let newzOrder = new Order({ user: userId });
  console.log("-pd-pfdokfjoisjfj");
  console.log(newzOrder);
  var getProductOrderedId = [];
  const overInStock = [];

  // console.log(quantity);
  // console.log(newzOrder);

  let getUser = await User.findById(userId);
  // console.log("This is here");
  // console.log(getUser);
  if (!getUser) {
    console.log("User not found");
    return ["User not found"];
  }

  const productFromStock = await Product.findById(productId);

  if (quantity === 0) {
    return ["Add some Product"];
  }
  // console.log(productFromStock);
  // console.log("AMOUNT: " + productFromStock.amount);
  // console.log("=======");
  // console.log(productFromStock);
  if (quantity > productFromStock.amount) {
    overInStock.push(productFromStock.product_name);
  }
  if (overInStock.length > 0) {
    return overInStock;
  }

  await Product.findByIdAndUpdate(productId, {
    amount: productFromStock.amount - quantity,
  });

  // console.log("========= xx ======");
  // console.log(x);

  const newOrdered = new OrderedProducts({
    product: productFromStock._id,
    orderId: newzOrder._id,
    quantity: quantity,
  });

  getProductOrderedId[0] = newOrdered._id;
  let getProductOrders = await Product.findById(productId);
  console.log("==;=;=");
  // console.log(getProductOrderedId);
  getProductOrders.orders.push(getProductOrderedId);

  await getProductOrders.save();

  let storeProduct = [];
  // storeProduct[0] = productId;
  storeProduct[0] = newOrdered._id;
  newzOrder.products.push(storeProduct);

  if (!getUser.orders) {
    getUser.orders = [];
  }

  getUser.orders.push(newzOrder._id);
  // console.log(getUser.orders);
  await getUser.save();

  await newzOrder.save();
  await newOrdered.save();

  console.log(newOrdered);
  return newzOrder;
};
