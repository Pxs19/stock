const mongoose = require("../configs/db");
const product = require("../models/product");
const Product = require("../models/product");
const User = require("../models/user");
const OrderedProducts = require("../models/productinorder");

// all products
exports.getProducts = async () => {
  const products = await Product.find();
  return products;
};

exports.getProductsByUserWhoadded = async (userId) => {
  const products = await Product.find({ user: userId });
  return products;
};

// by id product
exports.getProduct = async (id) => {
  const product = await Product.findById(id);
  return product;
};

//get product + order by productId
exports.getProductWithOrder = async (id) => {
  const productsOrders = await Product.find({ _id: id }).populate("orders");
  return productsOrders;
};

exports.addProduct = async (productData) => {
  const { product_name, price, amount, user } = productData;
  const newProduct = new Product({ product_name, price, amount, user });
  console.log(newProduct._id.valueOf().toString());
  let addProductId = [];
  addProductId.push(newProduct._id.valueOf().toString());
  console.log(addProductId);

  const findUser = await User.findById(user);
  findUser.products.push(addProductId);
  await findUser.save();
  // await User.findByIdAndUpdate(user, {
  //   products: addProductId,
  // });

  let product = await newProduct.save();

  return product;
};

exports.updateProduct = async (id, productData) => {
  const { product_name, price, amount, user } = productData;
  console.log(product_name);
  // const userId = userID // from token (wait jwt function 😗) ไม่ใส่ ? ใส่ดี

  //   check in db at service

  //   update product
  return await Product.findByIdAndUpdate(
    { _id: id },
    { product_name: product_name, price: price, amount: amount },
    { new: true }
  );
};

exports.deleteProduct = async (id) => {
  return await Product.findByIdAndDelete(id);
};

exports.getProductsByUserId = async (userId) => {
  const result = await Product.find({ user: userId });
  return result;
};
