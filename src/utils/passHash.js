const bcrypt = require("bcrypt");

function hashFunction(password) {
  //   console.log(process.env.SALT);
  const hash = bcrypt.hash(password, 10);
  console.log(hash);
  return hash;
}

module.exports = hashFunction;
