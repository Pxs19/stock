var router = require("express").Router();
const orderController = require("../controller/order.controller");
const passport = require("../configs/passport");
const roleGuard = require("../configs/roleguard");

// get all orders
router.get(
  "/",
  [passport, roleGuard(["admin", "user"])],
  orderController.getOrders
);
// add one product **use this methods
router.post(
  "/",
  [passport, roleGuard(["admin", "user"])],
  orderController.createOrder
);

module.exports = router;
