var router = require("express").Router();
const productController = require("../controller/product.controller");
const jwt = require("../configs/jwt");
const roleGD = require("../configs/roleguard");
const passport = require("../configs/passport");
const roleGuard = require("../configs/roleguard");

// get all product
// router.get(
//   "/",
//   [jwt.verifyToken, roleGD.checkRole("admin")],
//   productController.getProducts
// );
router.get(
  "/",
  [passport, roleGuard(["admin", "user"])],
  productController.getProducts
);
// find product by id
router.get(
  "/:id",
  [passport, roleGuard(["admin", "user"])],
  productController.getProduct
);
// find product added by userId
router.get(
  "/user/:id",
  [passport, roleGuard(["admin", "user"])],
  productController.getProductsByUserWhoadded
);
// add one product
router.post(
  "/",
  [passport, roleGuard(["admin", "user"])],
  productController.addProduct
);
// update product (PATCH or PUT)
router.patch(
  "/:id",
  [passport, roleGuard(["admin", "user"])],
  productController.updateProduct
);
// delete product by id
router.delete(
  "/:id",
  [passport, roleGuard(["admin", "user"])],
  productController.deleteProduct
);

// suck
router.get(
  "/:id/order",
  [passport, roleGuard(["admin", "user"])],
  productController.getProductWithOrder
);
//--> products/:id/orders
router.post(
  "/:id/order",
  [passport, roleGuard(["admin", "user"])],
  productController.uselessCreateOrder
);

module.exports = router;
