var express = require("express");
const mongoose = require("../configs/db");
const User = require("../models/user");
var router = express.Router();
const userController = require("../controller/user.controller");
const passport = require("../configs/passport");
const roleGuard = require("../configs/roleguard");

/* GET users listing. */
// router.get("/", async function (req, res, next) {
//   const users = await User.find();
//   res.send(users);
// });

// get all users
router.get(
  "/",
  [passport, roleGuard(["admin", "user"])],
  userController.getUsers
);

router.post("/", userController.addUser);

router.get(
  "/products",
  [passport, roleGuard(["admin", "user"])],
  userController.getProductsByUserId
);

router.patch(
  "/approve/:id",
  [passport, roleGuard(["admin"])],
  userController.adminApprove
);

module.exports = router;
