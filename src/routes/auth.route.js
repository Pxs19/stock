var express = require("express");
const mongoose = require("../configs/db");
const User = require("../models/user");
var router = express.Router();
const authController = require("../controller/auth.controller");

router.post("/register", authController.signUp);

router.post("/login", authController.login);

module.exports = router;
