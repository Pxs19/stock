const userRepository = require("../repository/user.repository");

exports.signUp = async (userData) => {
  return await userRepository.addUser(userData);
};

exports.login = async (userData) => {
  return await userRepository.login(userData);
};
