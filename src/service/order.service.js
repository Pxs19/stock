const { default: axios } = require("axios");
const mongoose = require("../configs/db");
const orderRepository = require("../repository/order.repository");
const { application } = require("express");

exports.getOrders = async (userId) => {
  return await orderRepository.getOrders(userId);
};

exports.createOrder = async (userId, orderData) => {
  //   return await orderRepository.createOrder(userId, orderData);
  return await orderRepository.createSuckOrder(userId, orderData);
};

// exports.payment = async (result) => {
//   const orderId = result._id;
//   const products = result.products;
//   let pricetopay = 0;
//   // map quantity with price
//   const productyay = products.map((products) => {
//     pricetopay = products.quantity * products.product.price;
//   });

//   // console.log(productyay);

//   const key =
//     "HftjRZ/rMJU1pQb4d2udp1mylYQA84gF0FygOELL/WcDNv9T9HC0cfkExGCVX858HzO5IloC3GRYUUDc/eYZptCbBv9lHVv1uYr1eonVvpjNsOzbIHlDSOXz4j/cg8NS36oPVpEsbodFBnxgnVeUDQVZaDPbnnOKATYsTMkeMuk=";
//   const oAuthpayload = {
//     key: key,
//     orderId: orderId.toString(),
//   };
//   // console.log(oAuthpayload);

//   // oAuth
//   const response = await axios.post(
//     "https://new-ops-poc.inet.co.th/uat/oauth/api/v1/oauth-token",
//     oAuthpayload
//   );

//   const oauth = response.data;
//   const oauth_accesstoken = oauth.data.token;

//   // ==== create transaction part ===
//   const configHeader = {
//     headers: { Authorization: `Bearer ${oauth_accesstoken}` },
//     "Content-Type": "application/json",
//   };

//   const transac_payload = {
//     key,
//     orderId: orderId.toString(),
//     orderDesc: "",
//     amount: pricetopay,
//     apUrl: "https://www.google.co.th",
//     regRef: "",
//     payType: "QR",
//   };

//   const transaction = await axios.post(
//     "https://new-ops-poc.inet.co.th/uat/api/v1/payment-transactions/access-token",
//     transac_payload,
//     configHeader
//   );

//   // console.log(transaction.data);

//   // ===== gen QR part ====
//   const accessTokenQR = transaction.data.data.accessToken;
//   // console.log(accessTokenQR);
//   const qrPayload = {
//     accessToken: `${accessTokenQR}`,
//   };
//   const qr_response = await axios.post(
//     "https://new-ops-poc.inet.co.th/uat/bbl/api/v1/qr-code/bbl",
//     qrPayload
//   );

//   // console.log(qr_response.data);

//   return qr_response.data;
// };
