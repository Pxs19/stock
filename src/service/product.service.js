const mongoose = require("../configs/db");
const productRepository = require("../repository/product.repository");
const orderRepository = require("../repository/order.repository");

exports.getProducts = async () => {
  return await productRepository.getProducts();
};

exports.getProduct = async (id) => {
  // check id is object ?
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return ["id is not a ObjectId"];
  } else {
    const product = await productRepository.getProduct(id);

    // if (!product) {
    //   return ["Product not found with the provided ID"];
    // }

    return product;
  }
};

exports.getProductsByUserWhoadded = async (id) => {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return ["id is not a ObjectId"];
  } else {
    const product = await productRepository.getProductsByUserWhoadded(id);
    return product;
  }
};

exports.getProductWithOrder = async (id) => {
  const productsOrders = await productRepository.getProductWithOrder(id);
  return productsOrders;
};

exports.addProduct = async (productData) => {
  return await productRepository.addProduct(productData);
};

exports.updateProduct = async (id, productData) => {
  return await productRepository.updateProduct(id, productData);
};

exports.deleteProduct = async (id) => {
  return await productRepository.deleteProduct(id);
};

exports.orderProduct = async (userId, orderData) => {
  return await orderRepository.createSuckOrder(userId, orderData);
};

exports.uselessCreateOrder = async (productId, userId, orderData) => {
  return await orderRepository.uselessCreateOrder(productId, userId, orderData);
};

exports.getProductsByUserId = async (userId) => {
  return await productRepository.getProductsByUserId(userId);
};
