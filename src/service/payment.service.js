const { default: axios } = require("axios");

exports.payment = async (data) => {
  console.log("Hello");
  const orderId = data._id;

  let pricetopay = 0;
  const productyay = data.products.map((products) => {
    pricetopay = products.quantity * products.product.price;
  });

  // gen oAuth
  const oAuth_token = await oAuth(orderId);
  console.log(oAuth_token);

  // create transaction
  const transaction_token = await createTransaction(
    orderId,
    pricetopay,
    oAuth_token
  );

  console.log(transaction_token);

  //   // gen QR
  const qrCode = await genQR(transaction_token);

  return qrCode;
};

const oAuth = async (orderId) => {
  const oAuthpayload = {
    key: process.env.KEY,
    orderId: orderId.toString(),
  };

  try {
    const response = await axios.post(
      "https://new-ops-poc.inet.co.th/uat/oauth/api/v1/oauth-token",
      oAuthpayload
    );

    const { data } = response.data;
    const { token } = data;

    return token;
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

const createTransaction = async (orderId, pricetopay, oauth_accesstoken) => {
  const configHeader = {
    headers: { Authorization: `Bearer ${oauth_accesstoken}` },
    "Content-Type": "application/json",
  };

  const transac_payload = {
    key: process.env.KEY,
    orderId: orderId.toString(),
    orderDesc: "",
    amount: pricetopay,
    apUrl: "https://www.google.co.th",
    regRef: "",
    payType: "QR",
  };

  try {
    const response = await axios.post(
      "https://new-ops-poc.inet.co.th/uat/api/v1/payment-transactions/access-token",
      transac_payload,
      configHeader
    );

    const { data } = response.data;
    const { accessToken } = data;

    return accessToken;
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

const genQR = async (qr_accesstoken) => {
  const qrPayload = {
    accessToken: `${qr_accesstoken}`,
  };
  try {
    const qr_response = await axios.post(
      "https://new-ops-poc.inet.co.th/uat/bbl/api/v1/qr-code/bbl",
      qrPayload
    );

    const { data } = qr_response;

    return data;
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};
