const userRepository = require("../repository/user.repository");
const mongoose = require("../configs/db");

exports.getUsers = async () => {
  return await userRepository.getUsers();
};

exports.addUser = async (userData) => {
  console.log(userData);
  return await userRepository.addUser(userData);
};

exports.adminApprove = async (userId, approveData) => {
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return ["id is not a ObjectId"];
  }
  return await userRepository.adminApprove(userId, approveData);
};
