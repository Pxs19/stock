const jwt = require("jsonwebtoken");
var roleGd = require("./roleguard");
const cookieParser = require("cookie-parser");

signToken = (payload) => {
  // console.log(payload);
  const token = jwt.sign(payload, process.env.JWT_KEY, { expiresIn: "1d" });
  return token;
};

verifyToken = (req, res, next) => {
  try {
    const token = req.headers.authorization.split("Bearer ")[1];
    const decode = jwt.verify(token, process.env.JWT_KEY);

    res.cookie("jwtToken", decode, { httpOnly: true, maxAge: 86400000 });

    return next();
  } catch (error) {
    return res.status(401).send({ message: "Auth fail" });
  }
};

module.exports = { signToken, verifyToken };
