const mongoose = require("mongoose");

const { DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS } = process.env;

mongoose
  .connect(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`, {
    // user: DB_USER,
    // password: DB_PASS,
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then(() => {
    console.log("DB Connected !!");
  })
  .catch((err) => {
    console.log("DB connect fail !!", err);
  });

module.exports = mongoose;
