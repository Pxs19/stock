FROM node:20-slim

RUN apt-get -y update && apt-get install -y openssl
RUN openssl version -v

WORKDIR /usr/src/app

COPY package.json ./

COPY . .

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]