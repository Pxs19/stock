## Description

stock

## > start mongodb with docker

```bash
    docker-compose up -d
```

## > Install

```bash
    $ npm install
```

## > start App

```bash
    $ npm run dev
```
